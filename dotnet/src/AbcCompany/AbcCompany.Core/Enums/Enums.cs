﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcCompany.Core.Enums
{
    public enum CompanyTypeViewEnum
    {
        Software = 1,
        Manufacturing = 2,
        Service = 3,
        Unknown = 0
    }

    public enum CompanyListFieldViewEnum
    {
        Name,
        Type,
        Email
    }

    public enum SortDirectionViewEnum
    {
        Ascending,
        Descending
    }

}

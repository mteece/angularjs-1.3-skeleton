﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcCompany.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class Result
    {
        private List<ErrorResponse> _errorList;

        public bool Success { get; set; }
        public string Message { get; set; }
        public int ErrorCode { get; set; }

        public List<ErrorResponse> Errors
        {
            get { return _errorList; }
            set { _errorList = value; }
        }

        public Result()
        {

        }


        public Result(List<ErrorResponse> errors, bool success = false)
        {
            this.Errors = errors.ToList();
            this.Success = success;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Result<T> : Result
    {
        public T Value { get; set; }

        public Result(bool success, T value) : base()
        {
            this.Value = value;
            this.Success = success;
        }

        public Result(T value, List<ErrorResponse> errors, bool success = false)
        {
            this.Value = value;
            this.Errors = errors.ToList();
            this.Success = success;
        }
    }

    public enum ErrorTypeEnum
    {
        /// <summary>
        /// Operating Systems, Core Framework etc.
        /// </summary>
        System,
        /// <summary>
        /// Application
        /// </summary>
        Application,
        /// <summary>
        /// Sql Server
        /// </summary>
        Database,
        /// <summary>
        /// 3rd Party Services, components etc.
        /// </summary>
        External
    }

    /// <summary>
    /// Application specific codes if different from integer.
    /// </summary>
    public enum ErrorCodeEnum
    { 
        Unknown
        // Application specific codes
    }

    /// <summary>
    /// Wrapper for model level errors.
    /// </summary>
    public class ErrorResponse
    {
        private string _messageField;
        private ErrorTypeEnum _errorTypeField;
        private int _codeField;

        public string Message
        {
            get { return this._messageField; }
            set { this._messageField = value; }
        }

        public ErrorTypeEnum ErrorType
        {
            get { return this._errorTypeField; }
            set { this._errorTypeField = value; }
        }

        public int ErrorCode
        {
            get { return this._codeField; }
            set { this._codeField = value; }
        }
    }
}

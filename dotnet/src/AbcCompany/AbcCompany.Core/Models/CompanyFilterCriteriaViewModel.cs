﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace AbcCompany.Core.Models
{
    using AbcCompany.Core.Enums;

    public class CompanyFilterCriteriaViewModel
    {

        /// <summary>
        /// The type of company to filter by
        /// </summary>
        public CompanyTypeViewEnum? CompanyType { get; set; }

        /// <summary>
        /// The company ID to filter by
        /// </summary>
        public int? CompanyId { get; set; }

        /// <summary>
        /// The search term(s) to filter by
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// The sort order of results
        /// </summary>
        [Required]
        public CompanyListFieldViewEnum SortField { get; set; }

        /// <summary>
        /// The sort direction of results
        /// </summary>
        [Required]
        public SortDirectionViewEnum SortDirection { get; set; }

        /// <summary>
        /// The page number of results
        /// </summary>
        [Required]
        public int Page { get; set; }

        /// <summary>
        /// The page size of results
        /// </summary>
        [Required]
        public int PageSize { get; set; }
    }
}
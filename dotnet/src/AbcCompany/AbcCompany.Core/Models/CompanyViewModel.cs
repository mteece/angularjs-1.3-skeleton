﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbcCompany.Core.Models
{
    using AbcCompany.Core.Enums;

    public class CompanyViewModel
    {
        public string Id { get; set; }
        public int Index { get; set; }
        public CompanyTypeViewEnum CompanyType { get; set; }
        public Guid Guid { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string[] Tags { get; set; }
    }
}
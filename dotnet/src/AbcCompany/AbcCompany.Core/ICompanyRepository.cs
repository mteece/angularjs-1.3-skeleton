﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcCompany.Core
{
    using AbcCompany.Core.Models;

    public interface ICompanyRepository
    {
        Result<List<CompanyViewModel>> GetCompanies(CompanyFilterCriteriaViewModel value);
        Result<CompanyViewModel> GetCompanyById(Guid id);
        Result<Guid> AddCompany(CompanyViewModel company);
        Result UpdateCompany(CompanyViewModel company);
    }
}

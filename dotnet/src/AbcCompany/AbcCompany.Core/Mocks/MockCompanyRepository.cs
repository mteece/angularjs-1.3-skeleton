﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcCompany.Core.Mocks
{
    using AbcCompany.Core.Models;
    using AbcCompany.Core.Enums;

    public class MockCompanyRepository : ICompanyRepository
    {
        private static List<CompanyViewModel> _companies;

        internal static List<CompanyViewModel> Companies { get { return _companies; } }

        static MockCompanyRepository()
        {
            _companies = new List<CompanyViewModel>()
            { 
                new CompanyViewModel {
                    Id = "55252b3e91b38d979382ac5a",
                    Index = 0,
                    Guid = Guid.Parse("70b9fd72-7fc4-4960-89e1-00cc88c877ea"),
                    CompanyType = CompanyTypeViewEnum.Unknown,
                    Company = "CEMENTION",
                    Email = "noreenpitts@cemention.com",
                    Phone = "+1 (919) 519-3006",
                    Address = "110 Irving Avenue, Hondah, Maine, 2863",
                    Latitude = -63.101972,
                    Longitude = -76.518422,
                    Tags = new string[7]{
                        "mollit",
                        "officia",
                        "occaecat",
                        "ex",
                        "voluptate",
                        "ex",
                        "aliqua"
                    }
                }, 
                new CompanyViewModel { 
                    Id = "55252b3e95b094bb6c76c176",
                    Index = 1,
                    Guid = Guid.Parse("ec73ca82-4b22-4acc-b74f-4fbed1420d80"),
                    CompanyType = CompanyTypeViewEnum.Unknown,
                    Company = "SLUMBERIA",
                    Email = "noreenpitts@slumberia.com",
                    Phone = "+1 (957) 473-2961",
                    Address = "776 Everett Avenue, Bodega, Wisconsin, 2517",
                    Latitude = -60.712052,
                    Longitude = 46.346805,
                    Tags = new string[7]{
                        "cupidatat",
                        "aute",
                        "proident",
                        "consectetur",
                        "eu",
                        "anim",
                        "anim"
                    }
                }, 
                new CompanyViewModel { 
                    Id = "55252b3eaadfd3a8d5724bb8", 
                    Index = 2, 
                    Guid = Guid.Parse("c9ab4f42-e7dc-41b8-9e39-5348cb04bca3"),
                    CompanyType = CompanyTypeViewEnum.Unknown,
                    Company = "DOGSPA",
                    Email = "noreenpitts@dogspa.com",
                    Phone = "+1 (861) 496-3907",
                    Address = "573 Lee Avenue, Nadine, Montana, 5880",
                    Latitude = 55.161171,
                    Longitude = -66.756093,
                    Tags = new string[7] { 
                        "nulla",
                        "ipsum",
                        "ipsum",
                        "eiusmod",
                        "elit",
                        "laborum",
                        "anim"
                    }
                } 
            };
        }

        public MockCompanyRepository()
        {
            // Initialize
        }

        public Result<List<CompanyViewModel>> GetCompanies(CompanyFilterCriteriaViewModel criteria)
        {
            IEnumerable<CompanyViewModel> companiesToReturn = _companies;
            if (criteria != null && criteria.Keywords != null && criteria.Keywords.Length > 0)
            {
                companiesToReturn = companiesToReturn.Where(s => s.Company.Contains(criteria.Keywords));
            }
            if (criteria.CompanyType != null)
            {
                if (criteria.CompanyType != CompanyTypeViewEnum.Unknown)
                {
                    companiesToReturn = companiesToReturn.Where(s => s.CompanyType == criteria.CompanyType.Value);
                }
            }

            return new Result<List<CompanyViewModel>>(true, companiesToReturn.ToList());
        }

        public Result<CompanyViewModel> GetCompanyById(Guid id)
        {
            var result = (CompanyViewModel)_companies.FirstOrDefault(s => s.Guid == id);
            return new Result<CompanyViewModel>(true, result);
        }

        public Result<Guid> AddCompany(CompanyViewModel company)
        {
            var nextId = this.GenerateId();
            company.Id = nextId;
            company.Guid = Guid.NewGuid();

            _companies.Add(company);
            return new Result<Guid>(true, company.Guid);
        }

        public Result UpdateCompany(CompanyViewModel company)
        {
            var existingCompany = _companies.FirstOrDefault(s => s.Guid == company.Guid);
            var loc = _companies.IndexOf(existingCompany);
            _companies[loc] = company;
            return new Result() { Success = true };
        }

        private string GenerateId()
        {
            var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 24)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AbcCompany.Web.Controllers
{
    using AbcCompany.Core.Models;
    using AbcCompany.Core;
    using AbcCompany.Core.Mocks;

    public class CompanyController : BaseApiController
    {
        public ICompanyRepository Repository { get; set; }

        public CompanyController() 
            : base() 
        {
            this.Repository = new MockCompanyRepository();  
        }

        [HttpPost]
        public Result<List<CompanyViewModel>> FindCompanies([FromBody] CompanyFilterCriteriaViewModel value)
        {
            return StatusResult(this.Repository.GetCompanies(value));
        }
        
        [HttpPost]
        public Result<CompanyViewModel> GetCompanyById(Guid id)
        {
            return StatusResult(this.Repository.GetCompanyById(id));
        }

        [HttpPost]
        public Result<Guid> AddCompany([FromBody]CompanyViewModel value)
        {
            if (this.ModelState.IsValid)
            {
                return StatusResult(this.Repository.AddCompany(value));
            }
            return StatusResult(new Result<Guid>(Guid.Empty, GetModelStateErrors(), false));
        }

        [HttpPost]
        public Result UpdateCompany(Guid id, [FromBody]CompanyViewModel value)
        {
            if (this.ModelState.IsValid)
            {
                return StatusResult(this.Repository.UpdateCompany(value));
            }
            return StatusResult(new Result(GetModelStateErrors(), false));
        }

        
    }
}

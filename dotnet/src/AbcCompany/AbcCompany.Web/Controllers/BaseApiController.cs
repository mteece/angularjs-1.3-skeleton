﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;

namespace AbcCompany.Web.Controllers
{
    using AbcCompany.Core;

    public abstract class BaseApiController : ApiController
    {
        private void FailStatusResultIfNecessary(HttpStatusCode statusCode, object content)
		{
			throw new HttpResponseException(new HttpResponseMessage
			{
				Content = new StringContent(
					JsonConvert.SerializeObject(content, Configuration.Formatters.JsonFormatter.SerializerSettings)
				),
				StatusCode = statusCode
			});
		}

        protected Result StatusResult(Result result, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            if (!result.Success)
            {
                FailStatusResultIfNecessary(statusCode, result);
            }
            return result;
        }

        protected Result<T> StatusResult<T>(Result<T> result, HttpStatusCode statusCode = HttpStatusCode.InternalServerError) where T : new()
        {
            if (!result.Success)
            {
                FailStatusResultIfNecessary(statusCode, result);
            }
            return result;
        }

        protected List<ErrorResponse> GetModelStateErrors()
        {
            var errorResponses = new List<ErrorResponse>();

            foreach (var fieldState in this.ModelState)
            {
                foreach (var err in fieldState.Value.Errors)
                {
                    errorResponses.Add(new ErrorResponse() {
                        ErrorCode = 0, 
                        ErrorType = ErrorTypeEnum.Application, 
                        Message = err.ErrorMessage 
                    });
                }
            }

            return errorResponses;
        }
    }
}

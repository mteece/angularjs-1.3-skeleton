﻿using System.Web;
using System.Web.Optimization;

namespace AbcCompany.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            /*
            
            bundles.UseCdn = true;
            BundleTable.EnableOptimizations = true;
            
            bundles.Add(new ScriptBundle("~/bundles/angularjs",
                 @"//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular.js"
                ).Include("~/Content/libs/angularjs/angular.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-ui-router",
                 @"//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.13/angular-ui-router.js"
                ).Include("~/Content/libs/angularjs/angular-ui-router.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-animate",
                @"//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-animate.js"
               ).Include("~/Content/libs/angularjs/angular-animate.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-messages",
                @"//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-messages.js"
               ).Include("~/Content/libs/angularjs/angular-messages.js"));

            bundles.Add(new ScriptBundle("~/bundles/ng-table",
                 @"//cdnjs.cloudflare.com/ajax/libs/ng-table/0.3.3/ng-table.js"
                ).Include("~/Content/libs/angularjs/ng-table.js"));

            bundles.Add(new ScriptBundle("~/bundles/ui-bootstrap",
                @"//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.12.1/ui-bootstrap.js"
               ).Include("~/Content/libs/angularjs/ui-bootstrap.js"));
        
            bundles.Add(new ScriptBundle("~/bundles/ui-bootstrap-tpls",
                @"//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.12.1/ui-bootstrap-tpls.js"
               ).Include("~/Content/libs/angularjs/ui-bootstrap-tpls.js"));
            */

            // Physical file system
            bundles.Add(new ScriptBundle("~/bundles/angular-uuid2"
               ).Include("~/Content/libs/angularjs/angular-uuid2.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/app-share")
                    .IncludeDirectory("~/Content/app-share/", "*.js", true)
            );

            bundles.Add(
                new ScriptBundle("~/bundles/abc-company-app")
                    .IncludeDirectory("~/Content/abc-company-app/", "*.js", true)
            );

            /*
            bundles.Add(new StyleBundle("~/bundles/css/bootstrap-css",
                @"//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
               ).Include("~/Content/stylesheets/bootstrap.min.css"));

            */
            
            bundles.Add(new StyleBundle("~/bundles/css/css-main").Include(
                      "~/Content/stylesheets/style.css"));
           
        }
    }
}
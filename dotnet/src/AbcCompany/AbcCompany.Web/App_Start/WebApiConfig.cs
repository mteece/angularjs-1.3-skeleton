﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Web.Http.Validation.Providers;

namespace AbcCompany.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //CamelCase json
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Major bug in Web API 2 http://aspnetwebstack.codeplex.com/workitem/270
            GlobalConfiguration.Configuration.Services.RemoveAll(
                typeof(System.Web.Http.Validation.ModelValidatorProvider),
                     v => v is InvalidModelValidatorProvider);

            //Adds $type property to JSON objects, but allows proper serialization of abstract collections (like Checklist Items)
            config.Formatters.JsonFormatter.SerializerSettings.TypeNameHandling = TypeNameHandling.Objects;

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}

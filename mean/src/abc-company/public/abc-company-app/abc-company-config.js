angular.module('abc-company-config', [])

    .constant('cfg', {
        'API_ROOT': API_ROOT || '',
        'URL_ROOT': URL_ROOT || '',                         // Defined in markup from host or null
        'INTERFACE_LANGUAGE': 'en',                         // default interface language (iso code: e.g: 'en')
        'MAX_INPUT_LENGTH': 255,                            //
        'MAX_TEXTAREA_LENGTH': 1024                         //
    })

    .constant('serialization', {
        'TYPE_ABC_COMPANY_VIEW_MODEL': 'AbcCompany.Core.Models.CompanyViewModel, AbcCompany.Core'
    });
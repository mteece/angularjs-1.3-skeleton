angular.module('abcCompanyApp', [
    'ngMessages',
    'ngAnimate',
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.tpls',
    'angularUUID2',
    // Shared
    'app-utils',
    'app-filters',
    // App specific
    'abc-company-config',
    'abc-company-repository',
    'abc-company-list',
    'abc-company-detail',
    'abc-company-add'
])

.config(function ($stateProvider, $urlRouterProvider, cfg) {
    'use strict';

    $urlRouterProvider.otherwise('/');

    $stateProvider.state('abccompany', {
        title: 'ABC Company',
        views: {
            '': {
                templateUrl: cfg.URL_ROOT + '/abc-company-app/abc-company.html',
                resolve: {

                },
                controller: function ($scope, $state, $modal, abcCompanyRepository, utils) {

                    $scope.view = {
                        editMode: false
                    };

                    $scope.toggleEditMode = function() {
                        var editMode = $scope.view.editMode;
                        $scope.view.editMode = (editMode) ? false : true;
                        console.log('Edit mode:', $scope.view.editMode);
                    };

                    $scope.actions = angular.extend(utils, {

                    });

                }

            }
        }
    });
 })

angular.module('abc-company-list', [
    'ui.router',
    'ngTable'
]).config(function ($stateProvider, $urlRouterProvider, cfg) {
    'use strict';
    $stateProvider.state('abccompany.list', {
        url: '/',
        views: {
            'main-body@abccompany': {

                templateUrl: cfg.URL_ROOT + '/abc-company-app/abc-company-list/abc-company-list.html',
                resolve: {
                    abcCompanyListData: function($stateParams, abcCompanyRepository) {
                        var params = {};

                        return abcCompanyRepository.getAbcCompanyData(params).then(function (resp) {
                            console.log('List data: ', resp);
                            return resp.data.value;
                        });
                    }
                },
                controller: function ($scope, $state, $filter, abcCompanyRepository, abcCompanyListData, ngTableParams) {

                    $scope.tableView = {};
                    $scope.abcCompanyListData = abcCompanyListData;
                    $scope.actions = angular.extend({}, $scope.actions, {
                        // Table and view level functions
                    });

                    $scope.tableView.rowStatus = {
                        isopen: false
                    };

                    // ngTable sorting
                    $scope.tableParams = new ngTableParams({
                        count: $scope.abcCompanyListData.length,          // count per page
                        sorting: {
                            name: 'asc'     // initial sorting
                        }
                    }, {
                        counts: [],
                        total: $scope.abcCompanyListData.length, // length of data
                        getData: function($defer, params) {
                            var data = $scope.abcCompanyListData;
                            var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        },
                        $scope: { $data: {} }
                    });

                }
            }
        }
    });
});

angular.module('abc-company-detail', [
    'ui.router',
    //
    'abc-company-config',
    'abc-company-repository',
    'abc-company-add'
]).config(function ($stateProvider, cfg) {
    'use strict';
    $stateProvider.state('abccompany.detail', {
        title: '',
        url: '/detail/:id?from',
        views: {
            'main-body': {
                templateUrl: cfg.URL_ROOT + '/abc-company-app/abc-company-detail/abc-company-detail-layout.html',
                resolve: {
                    seedAbcCompany: function($stateParams, abcCompanyRepository) {
                        var params = {};
                        return abcCompanyRepository.getNewAbcCompany(params).then(function(resp){
                            return resp.data.value;
                        });
                    },
                    abcCompanyData: function ($stateParams, abcCompanyRepository, abcCompanyAdd, seedAbcCompany) {
                        if ($stateParams.id === 'add') {
                            abcCompanyAdd.abcCompany = seedAbcCompany;
                            return abcCompanyAdd.abcCompany;
                        }

                        var idToLoad, isCopying;
                        if ($stateParams.id === 'copy') {
                            idToLoad = $stateParams.from; //use ID from query
                            isCopying = true;
                        } else {
                            idToLoad = $stateParams.id;
                            isCopying = false;
                        }

                        return abcCompanyRepository.getAbcCompany(idToLoad).then(function (resp) {
                            console.log('response:', resp);

                            if (resp.data.value && isCopying) {
                                //establish this as a new company to be added:
                                resp.data.value._id = '';
                                resp.data.value.guid = '';
                            }

                            return resp.data.value;
                        });
                    }
                },
                controller: function ($state, $scope, $stateParams, abcCompanyData, abcCompanyRepository, cfg) {

                    $scope.cfg = cfg;
                    $scope.$parent.abcCompany = abcCompanyData; // FIXME: there's a better way than $scope.$parent for breadcrumb

                    var isParamAnAdd = $stateParams.id === 'add' ? true : false;
                    var isParamCopy = $stateParams.from ? true : false;

                    // Properly enable controls on the bottom in footer.
                    $scope.view = angular.extend({}, $scope.view, {
                        editMode: isParamAnAdd,
                        addMode: isParamAnAdd, // for breadcrumb
                        copyMode: isParamCopy  // for breadcrumb
                    });

                    // Duplicate is essentially an edit mode
                    if ($scope.view.copyMode) {
                        $scope.view.editMode = true;
                    }
                }
            },
            'main-content@abccompany.detail': {
                templateUrl: cfg.URL_ROOT + '/abc-company-app/abc-company-detail/abc-company-detail.html'
            },
            'main-breadcrumb@abccompany.detail': {
                templateUrl: cfg.URL_ROOT + '/abc-company-app/abc-company-detail/abc-company-detail-breadcrumb.html'
            },

            'main-footer@abccompany.detail': {
                templateUrl: cfg.URL_ROOT + '/abc-company-app/abc-company-detail/abc-company-detail-footer.html',
                controller: function ($state, $scope, $stateParams, abcCompanyRepository, cfg) {

                    $scope.actions = angular.extend({}, $scope.actions, {
                        edit: function () {
                            $scope.view.editMode = true;
                            $scope.backupCompany = angular.copy($scope.abcCompany);
                            console.log('Action edit mode abcCompany');
                        },
                        cancelEdit: function () {
                            if ($scope.formSurvey.$dirty) {
                                // Handle any application specific messaging here.
                            }

                            $scope.view.editMode = false;
                            angular.copy($scope.formAbcCompanyDetail, $scope.abcCompany); //restore company from the backup
                            $scope.formAbcCompanyDetail.$setPristine();
                        },
                        save: function () {
                            console.log('Action save abcCompany');
                            // ngForm submit event
                            var isAdding = ($scope.abcCompany.id === 0 || $scope.abcCompany.guid === "00000000-0000-0000-0000-000000000000");

                            if ($scope.formAbcCompanyDetail.$valid) {
                                if (isAdding) {
                                    abcCompanyRepository.addAbcCompany($scope.abcCompany).then(function (resp) {
                                        console.log(resp);
                                        //alert('Saved');
                                        $scope.formAbcCompanyDetail.$setSubmitted();
                                        $scope.formAbcCompanyDetail.$setPristine();
                                        $scope.view.editMode = false;

                                        //redirect to newly saved record's detail page
                                        $state.go('abccompany.detail', { id: resp.data.value });
                                    });
                                } else {
                                    abcCompanyRepository.updateAbcCompany($scope.abcCompany.guid, $scope.abcCompany).then(function (resp) {
                                        console.log(resp);
                                        //alert('Saved');
                                        $scope.formAbcCompanyDetail.$setSubmitted();
                                        $scope.formAbcCompanyDetail.$setPristine();
                                        $scope.view.editMode = false;
                                    });
                                }
                            }
                        },
                        'delete': function () {

                            $scope.view.editMode = false;
                            $state.go('abccompany.list', null, { reload: true });

                        }
                    });

                }
            }
        }
    });
});
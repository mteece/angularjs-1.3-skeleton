angular.module('abc-company-repository', [
])

.factory('abcCompanyRepository', function ($http, $cacheFactory, $q, cfg, serialization, uuid2) {
    'use strict';

    function getDummyHttpCall(successValueFunc) {
        var deferred = $q.defer();

        setTimeout(function() {
            deferred.notify('Get Dummy Http Call');

            var successValue = {
                data: {
                    value: successValueFunc()
                }
            };

            deferred.resolve(successValue);
            //deferred.reject( successValue); We always have success

        }, 10);

        return deferred.promise;
    }

    function newDummyListData() {
        return [
            {
                "id": "55252b3e91b38d979382ac5a",
                "index": 0,
                "guid": "70b9fd72-7fc4-4960-89e1-00cc88c877ea",
                "company": "CEMENTION",
                "email": "noreenpitts@cemention.com",
                "phone": "+1 (919) 519-3006",
                "address": "110 Irving Avenue, Hondah, Maine, 2863",
                "latitude": -63.101972,
                "longitude": -76.518422,
                "tags": [
                    "mollit",
                    "officia",
                    "occaecat",
                    "ex",
                    "voluptate",
                    "ex",
                    "aliqua"
                ]
            },
            {
                "id": "55252b3e95b094bb6c76c176",
                "index": 1,
                "guid": "ec73ca82-4b22-4acc-b74f-4fbed1420d80",
                "company": "SLUMBERIA",
                "email": "noreenpitts@slumberia.com",
                "phone": "+1 (957) 473-2961",
                "address": "776 Everett Avenue, Bodega, Wisconsin, 2517",
                "latitude": -60.712052,
                "longitude": 46.346805,
                "tags": [
                    "cupidatat",
                    "aute",
                    "proident",
                    "consectetur",
                    "eu",
                    "anim",
                    "anim"
                ]
            },
            {
                "id": "55252b3eaadfd3a8d5724bb8",
                "index": 2,
                "guid": "c9ab4f42-e7dc-41b8-9e39-5348cb04bca3",
                "company": "DOGSPA",
                "email": "noreenpitts@dogspa.com",
                "phone": "+1 (861) 496-3907",
                "address": "573 Lee Avenue, Nadine, Montana, 5880",
                "latitude": 55.161171,
                "longitude": -66.756093,
                "tags": [
                    "nulla",
                    "ipsum",
                    "ipsum",
                    "eiusmod",
                    "elit",
                    "laborum",
                    "anim"
                ]
            },
            {
                "id": "55252b3e1c699c197c7795a3",
                "index": 3,
                "guid": "cf48df9e-c65a-4193-9c35-ae80f49c35c7",
                "company": "QUALITERN",
                "email": "noreenpitts@qualitern.com",
                "phone": "+1 (905) 427-2335",
                "address": "739 Linden Street, Allendale, Alaska, 9484",
                "latitude": -85.714938,
                "longitude": -20.999143,
                "tags": [
                    "do",
                    "velit",
                    "eiusmod",
                    "ut",
                    "nulla",
                    "irure",
                    "nisi"
                ]
            },
            {
                "id": "55252b3e191667129ea945b0",
                "index": 4,
                "guid": "39bab5db-6367-44dd-b669-a466210f7057",
                "company": "PORTICO",
                "email": "noreenpitts@portico.com",
                "phone": "+1 (920) 518-3781",
                "address": "508 Emerald Street, Frystown, Mississippi, 6472",
                "latitude": 34.375166,
                "longitude": -88.783771,
                "tags": [
                    "id",
                    "proident",
                    "laborum",
                    "consequat",
                    "in",
                    "consequat",
                    "tempor"
                ]
            }
        ];
    }

    function newDummyData() {
        return {
            "id": "", //uuid2.newuuid(),
            "index": 0,
            "guid": "00000000-0000-0000-0000-000000000000", //uuid2.newguid(),
            "company": "",
            "email": "",
            "phone": "",
            "address": "",
            "latitude": 0.0,
            "longitude": 0.0,
            "tags": []
        };
    }

    function getDummyData() {
        return {
            "id": "55252b3e191667129ea945b0",
            "index": 4,
            "guid": "39bab5db-6367-44dd-b669-a466210f7057",
            "company": "PORTICO",
            "email": "noreenpitts@portico.com",
            "phone": "+1 (920) 518-3781",
            "address": "508 Emerald Street, Frystown, Mississippi, 6472",
            "latitude": 34.375166,
            "longitude": -88.783771,
            "tags": [
                "id",
                "proident",
                "laborum",
                "consequat",
                "in",
                "consequat",
                "tempor"
            ]
        };
    }

    var repo = {

        // Functions go here.

        getAbcCompanyData: function () {
            /*
            // Mock data
            return getDummyHttpCall(function () {
                return newDummyListData();
            });
            */
            var filterCriteria = {
                Keywords: "",
                SortDirection: "Ascending",
                SortField: "Name",
                Page: 1,
                PageSize: 1000
            };
            return $http.post(cfg.API_ROOT + '/api/company/FindCompanies', filterCriteria);
        },

        getNewAbcCompany: function (params) {
            return getDummyHttpCall(function () {
                return newDummyData();
            });
        },

        getAbcCompany: function (id) {
            /*
            // Mock data
            return getDummyHttpCall(function () {
                return getDummyData();
            });
            */
            return $http.post(cfg.API_ROOT + '/api/company/GetCompanyById/' + id);
        },

        addAbcCompany: function (company) {
            return $http.post(cfg.API_ROOT + '/api/company/AddCompany', company);
        },

        updateAbcCompany: function(guid, company) {
            return $http.post(cfg.API_ROOT + '/api/company/UpdateCompany/' + guid, company);
        }
    };

    return repo;
});
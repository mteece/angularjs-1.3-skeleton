angular.module('app-utils', [])

.factory('utils', function() {
    'use strict';
    var utils = {};

    utils.moveUp = function (arr, value) {
        var index = arr.indexOf(value), newPos = index - 1;
        if (index < 1) { return; } //Can't move up
        arr.splice(index, 1);
        arr.splice(newPos, 0, value);
    };

    utils.moveDown = function (arr, value) {
        var index = arr.indexOf(value), newPos = index + 1;
        if (index === -1) { return; }
        if (index >= arr.length) { return; }
        arr.splice(index, 1);
        arr.splice(newPos, 0, value);
    };

    utils.remove = function (arr, value) {
        var index = arr.indexOf(value);
        if (index > -1) {
            arr.splice(index, 1);
        }
    };

    utils.moveTo = function (arr, value, ordinal) {
        var index = arr.indexOf(value),
            newPos = ordinal,
            temp = arr[newPos];
        if (index === -1) { return; }
        if (index === newPos) { return; }
        if (newPos >= arr.length) {
            // Move to the end
            newPos = arr.length - 1;
            temp = arr[newPos];
        }
        if(newPos < 0) {
            // Move to front
            newPos = 0;
            temp = arr[newPos];
        }
        arr.splice(newPos, 1, value); // Remove 1 item at newPos insert value
        arr.splice(index, 1, temp); // Remove 1 item at index, insert temp
    };

    utils.insertBefore = function(arr, value, ordinal) {
        var index = ordinal;
        // if <= -1 insert at 0
        if (index <= -1) { index = 0; }
        arr.splice(index, 0, value); // Remove 0 items at index, insert value
    };

    utils.insertAfter = function(arr, value, ordinal) {
        var index = ordinal;
        // if <= -1 insert after 0
        if (index <= -1) { index = 0; }
        index++;
        arr.splice(index, 0, value); // Remove 0 items at index, insert value
    };

    utils.duplicate = function(arr, item) {
        // keep in mind we need distinct objects, no references.
        var clone =  angular.copy(item),
            index = arr.indexOf(item),
            pos = index;
        if(index === -1) { return; }
        arr.splice(pos, 0, clone);
    };

    utils.contains = function(source, str, startIndex) {
        return ''.indexOf.call(source, str, startIndex) !== -1;
    };

    utils.parseBoolean = function(string) {
        var bool;
        bool = (function() {
            switch (false) {
                case string.toLowerCase() !== 'true':
                    return true;
                case string.toLowerCase() !== 'false':
                    return false;
            }
        })();
        if (typeof bool === "boolean") {
            return bool;
        }
        return void 0;
    };

    return utils;
    });

/**
 * Created by mteece on 11/10/2014.
 */
angular.module('app-filters', [])

/*
 *	This filter provides string variable replacement similar to C# string.Format("{0}", "something");
 *	Usage: {{ "From model: {0}; and a constant: {1};" | format:model.property:"constant":...:... }}
 */
.filter("format", function () {
    'use strict';
    return function (input) {
        var args = arguments;
        return input.replace(/\{(\d+)\}/g, function (match, capture) {
            return args[1 * capture + 1];
        });
    };
});
